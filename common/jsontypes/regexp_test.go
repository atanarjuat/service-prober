package jsontypes

import (
	"encoding/json"
	"testing"
)

type testType struct {
	Rx1 Regexp `json:"rx1"`
	Rx2 Regexp `json:"rx2"`
}

func TestRegexp(t *testing.T) {
	data := `{"rx1": "^foo.*"}`

	var tt testType
	if err := json.Unmarshal([]byte(data), &tt); err != nil {
		t.Fatal(err)
	}
	if !tt.Rx1.IsSet() {
		t.Fatal("IsSet() returns false when it shouldn't")
	}
	if tt.Rx2.IsSet() {
		t.Fatal("IsSet() returns true when it shouldn't")
	}
}
