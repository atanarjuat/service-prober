package vars

import (
	"fmt"
	"os"
	"reflect"
	"strings"
)

func lookupVar(m map[string]interface{}, key string) (string, bool) {
	parts := strings.Split(key, ".")
	var cur interface{} = m

	for _, k := range parts {
		switch t := cur.(type) {
		case map[string]interface{}:
			elem, ok := t[k]
			if !ok {
				return "", false
			}
			cur = elem
		default:
			cur = t
		}
	}

	// Let fmt do the stringification.
	return fmt.Sprintf("%v", cur), true
}

// Replace shell-like variables in a string, possibly accessing
// recursive map elements using a.b.c notation.
func expandVars(s string, lookup map[string]interface{}) (string, error) {
	var undefined []string
	result := os.Expand(s, func(key string) string {
		value, ok := lookupVar(lookup, key)
		if !ok {
			undefined = append(undefined, key)
			return ""
		}
		return value
	})
	if len(undefined) > 0 {
		return "", fmt.Errorf("undefined references: %s", strings.Join(undefined, ", "))
	}
	return result, nil
}

func expandVarsSlice(v reflect.Value, lookup map[string]interface{}) (reflect.Value, error) {
	result := reflect.MakeSlice(v.Type(), 0, v.Len())

	for i := 0; i < v.Len(); i++ {
		f := v.Index(i)
		fv, err := getValue(f, f.Type(), lookup)
		if err != nil {
			return v, err
		}
		result = reflect.Append(result, fv)
	}

	return result, nil
}

func expandVarsMap(v reflect.Value, lookup map[string]interface{}) (reflect.Value, error) {
	result := reflect.MakeMap(v.Type())

	for _, k := range v.MapKeys() {
		f := v.MapIndex(k)
		fv, err := getValue(f, f.Type(), lookup)
		if err != nil {
			return v, err
		}
		result.SetMapIndex(k, fv)
	}

	return result, nil
}

func expandVarsObj(v reflect.Value, lookup map[string]interface{}) (result reflect.Value, err error) {
	// Create a copy of the original object, and retain a pointer to it
	// that we'll return at the end. In order to do so, we first need to
	// figure out if the value is a pointer or not, and do some
	// book-keeping to later return the appropriate value type.
	orig := v
	var deref bool
	if v.Type().Kind() == reflect.Ptr {
		v = v.Elem()
		deref = true
	}
	vt := v.Type()
	result = reflect.New(vt)
	out := result.Elem()

	// Go through public fields and call getValue recursively on them.
	for i := 0; i < v.NumField(); i++ {
		f := v.Field(i)
		ft := vt.Field(i)
		outf := out.Field(i)

		if !f.CanInterface() {
			// The type has unexported fields, so we can't modify
			// it for fear of disrupting internal state. Just
			// return the original value unchanged.
			return orig, nil
		}

		fv, err := getValue(f, ft.Type, lookup)
		if err != nil {
			return orig, err
		}
		outf.Set(fv)
	}

	if !deref {
		return out, nil
	}
	return result, nil
}

// getValue returns a copy of the original object referenced by the
// reflect.Value, applying the expandVars function to all string values
// (either struct fields, or map values) recursively contained in the object.
func getValue(f reflect.Value, ft reflect.Type, lookup map[string]interface{}) (reflect.Value, error) {
	result := f
	switch ft.Kind() {
	case reflect.Struct:
		fv, err := expandVarsObj(f, lookup)
		if err != nil {
			return result, err
		}
		result = fv
	case reflect.Ptr:
		if !f.IsNil() {
			fv, err := expandVarsObj(f, lookup)
			if err != nil {
				return result, err
			}
			result = fv
		}
	case reflect.Map:
		if !f.IsNil() {
			fv, err := expandVarsMap(f, lookup)
			if err != nil {
				return result, err
			}
			result = fv
		}
	case reflect.Slice:
		if f.Len() > 0 {
			fv, err := expandVarsSlice(f, lookup)
			if err != nil {
				return result, err
			}
			result = fv
		}
	case reflect.String:
		s, err := expandVars(f.String(), lookup)
		if err != nil {
			return result, err
		}
		result = reflect.ValueOf(s)
	}
	return result, nil
}

// Expand shell-like variable references (from 'lookup') into string fields
// and map values of 'obj', recursively.
//
// The recursion will navigate maps and public fields of structs, but will
// leave objects with unexported fields untouched so as not to disrupt complex
// types with internal private state.
//
// This results in the most intuitive behavior where calling Expand on a
// value of the following type:
//
// type MyType struct {
//    Name string
//    Pattern *regexp.Regexp
// }
//
// will do the sensible thing of processing Name but leaving Pattern alone
// (the *regexp.Regexp pointer will be copied unmodified to the result
// object).
//
func Expand(obj interface{}, lookup map[string]interface{}) (interface{}, error) {
	// Expand is just a wrapper around getValue, using API-friendly
	// interface{} types.
	v := reflect.ValueOf(obj)
	result, err := getValue(v, v.Type(), lookup)
	if err != nil {
		return nil, err
	}
	return result.Interface(), nil
}

// Merge two maps (not recursively, just at the top-level).
func merge(a, b map[string]interface{}) map[string]interface{} {
	m := make(map[string]interface{})
	for k, v := range a {
		m[k] = v
	}
	for k, v := range b {
		m[k] = v
	}
	return m
}

func crossProductMaps(key string, values []interface{}, input []map[string]interface{}) []map[string]interface{} {
	var out []map[string]interface{}

	for _, value := range values {
		for _, inp := range input {
			out = append(out, merge(inp, map[string]interface{}{key: value}))
		}
	}
	return out
}

func CrossProduct(loopVars []string, vars map[string]interface{}) ([]map[string]interface{}, error) {
	var out []map[string]interface{}
	out = append(out, vars)

	for _, loopVar := range loopVars {
		valuesIntf, ok := vars[loopVar]
		if !ok {
			return nil, fmt.Errorf("unknown loop variable '%s'", loopVar)
		}

		// Iterate over the elements of the list, using
		// reflection to avoid having to know the type of the
		// list entries.
		v := reflect.ValueOf(valuesIntf)
		if v.Type().Kind() != reflect.Slice {
			return nil, fmt.Errorf("variable '%s' is not a list", loopVar)
		}
		var values []interface{}
		for i := 0; i < v.Len(); i++ {
			values = append(values, v.Index(i).Interface())
		}

		out = crossProductMaps(loopVar, values, out)
	}

	return out, nil
}
