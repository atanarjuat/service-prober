module git.autistici.org/ai3/tools/service-prober

go 1.15

require (
	github.com/PuerkitoBio/goquery v1.8.0
	github.com/prometheus/client_golang v1.11.0
)
