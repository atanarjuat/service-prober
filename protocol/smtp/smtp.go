package smtp

import (
	"context"
	"crypto/tls"
	"errors"
	"fmt"
	"log"
	"net"
	"net/smtp"

	"git.autistici.org/ai3/tools/service-prober/protocol/dns"
)

type Conn struct {
	hostname string
	client   *smtp.Client
}

func Dial(ctx context.Context, addr string, conf *tls.Config, log *log.Logger, dnsMap map[string]string) (*Conn, error) {
	hostname, _, err := net.SplitHostPort(addr)
	if err != nil {
		return nil, err
	}
	dialer := dns.NewDNSOverrideDialer(dnsMap, conf)
	nc, err := dialer.DialTLSContext(ctx, "tcp", addr)
	if err != nil {
		return nil, err
	}

	// Set the context deadline on the connection.
	if deadline, ok := ctx.Deadline(); ok {
		nc.SetDeadline(deadline)
	}

	// Ensure that canceling the context will immediately close
	// the connection.
	go func() {
		<-ctx.Done()
		nc.Close()
	}()

	log.Printf("established SMTP+SSL connection to %s", nc.RemoteAddr().String())
	client, err := smtp.NewClient(newLogConn(nc, log), hostname)
	if err != nil {
		nc.Close()
		return nil, err
	}

	return &Conn{
		client:   client,
		hostname: hostname,
	}, nil
}

func (c *Conn) Close() error {
	defer c.client.Close()
	return c.client.Quit()
}

func (c *Conn) SendMail(username, password, from string, to []string, msg []byte) error {
	auth := newPlainAuth("", username, password, c.hostname)
	if ok, _ := c.client.Extension("AUTH"); !ok {
		return errors.New("server does not support AUTH extension")
	}
	if err := c.client.Auth(auth); err != nil {
		return fmt.Errorf("SMTP authentication error: %w", err)
	}

	if err := c.client.Mail(from); err != nil {
		return fmt.Errorf("SMTP error (MAIL): %w", err)
	}
	for _, addr := range to {
		if err := c.client.Rcpt(addr); err != nil {
			return fmt.Errorf("SMTP error (RCPT): %w", err)
		}
	}
	w, err := c.client.Data()
	if err != nil {
		return fmt.Errorf("SMTP error (DATA): %w", err)
	}
	if _, err := w.Write(msg); err != nil {
		return fmt.Errorf("SMTP error while writing message body: %w", err)
	}
	if err := w.Close(); err != nil {
		return fmt.Errorf("SMTP error while writing message body: %w", err)
	}
	return nil
}

// This is almost a copy of net/smtp/auth.go, but without the TLS
// check. The net/smtp package attempts to autodetect TLS usage by
// checking if the net.Conn it got is actually a *tls.Conn. Since we
// wrap our connection in the logging wrapper, this check fails, and
// net/smtp.PlainAuth will refuse to authenticate, thinking the
// connection is unencrypted.
type plainAuth struct {
	identity, username, password string
	host                         string
}

func newPlainAuth(identity, username, password, host string) smtp.Auth {
	return &plainAuth{identity, username, password, host}
}

func (a *plainAuth) Start(server *smtp.ServerInfo) (string, []byte, error) {
	// We know we have TLS.
	if server.Name != a.host {
		return "", nil, errors.New("wrong host name")
	}
	resp := []byte(a.identity + "\x00" + a.username + "\x00" + a.password)
	return "PLAIN", resp, nil
}

func (a *plainAuth) Next(fromServer []byte, more bool) ([]byte, error) {
	if more {
		// We've already sent everything.
		return nil, errors.New("unexpected server challenge")
	}
	return nil, nil
}
