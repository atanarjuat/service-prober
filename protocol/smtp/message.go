package smtp

import (
	"bytes"
	"crypto/rand"
	"encoding/hex"
	"fmt"
	"strings"
	"text/template"
	"time"
)

var (
	msgText = `From: {{.From}}
To: {{.To}}
Date: {{.Date}}
Subject: [diagnostics] Test message #{{.UniqueID}}
Message-ID: {{.MessageID}}
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 7bit
Content-Language: en-US
Precedence: bulk
Errors-To: {{.ErrorsTo}}

{{.Body}}

These messages are being sent by a service-prober instance, please
do not reply to them. If you are receiving them in error, do
contact <{{.ContactAddr}}> and let them know.

`
	msgTpl = template.Must(template.New("msg").Parse(msgText))

	defaultBody = "This is a test message, please ignore!"
)

func NewTestMessage(from, to, body string) (string, []byte) {
	if body == "" {
		body = defaultBody
	}
	domain := strings.Split(from, "@")[1]
	uniqueID := genUniqueID()
	messageID := fmt.Sprintf("<test-%s@%s>", uniqueID, domain)
	date := time.Now().Format(time.RFC1123Z)
	var buf bytes.Buffer
	msgTpl.Execute(&buf, map[string]interface{}{
		"From":        from,
		"To":          to,
		"Date":        date,
		"MessageID":   messageID,
		"UniqueID":    uniqueID,
		"ErrorsTo":    "noreply@" + domain,
		"ContactAddr": "postmaster@" + domain,
		"Body":        body,
	})

	return messageID, buf.Bytes()
}

func genUniqueID() string {
	var b [16]byte
	if _, err := rand.Read(b[:]); err != nil {
		panic(fmt.Sprintf("bad crypto read: %v", err))
	}
	return hex.EncodeToString(b[:])
}
