package http

import (
	"context"
	"crypto/tls"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"mime"
	"net/http"
	"net/http/cookiejar"
	"net/url"
	"strings"

	"github.com/PuerkitoBio/goquery"

	"git.autistici.org/ai3/tools/service-prober/protocol/dns"
)

const userAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36"

// Browser pretends (badly) to emulate a browser, to the extent that
// it keeps cookies across requests. It maintains state with the last
// browsed page, so that one can find elements in it using jquery-like
// CSS selector syntax, and click links or submit forms.
type Browser struct {
	client *http.Client

	lastResponse *Response
}

// NewBrowser creates a new Browser with the given tls configuration. Its
// debugging output will be sent to the specified Logger.
func NewBrowser(tlsConfig *tls.Config, debug *log.Logger, dnsMap map[string]string) *Browser {
	// The default http.Transport is good, make a copy of it (by peeking
	// at its implementation). We need a copy due to the potential
	// customization of the TLS config. Use the DialContext +
	// TLSClientConfig combination for dialing new connections, so that
	// DNS overrides also work with plain HTTP.
	transport := *(http.DefaultTransport.(*http.Transport))
	transport.TLSClientConfig = tlsConfig
	transport.DialContext = dns.NewDNSOverrideDialer(dnsMap, nil).DialContext
	jar, _ := cookiejar.New(nil)
	client := &http.Client{
		Jar:       jar,
		Transport: newLoggedTransport(&transport, debug),
	}
	return &Browser{
		client: client,
	}
}

type RequestOptions struct {
	Args    url.Values
	Headers map[string]string
}

// Response is an abridged version of the http.Response.
type Response struct {
	URL        *url.URL
	StatusCode int
	Header     http.Header
	Body       string
	Doc        *goquery.Document
}

func getContentType(resp *http.Response) string {
	if ct := resp.Header.Get("Content-Type"); ct != "" {
		if t, _, err := mime.ParseMediaType(ct); err == nil {
			return t
		}
	}
	return "text/plain"
}

// Open a new page, discarding the current state.
func (b *Browser) Open(ctx context.Context, method, uri string, opts *RequestOptions) (*Response, error) {
	var reqBody io.Reader
	var ctype string
	if method == "POST" && opts != nil {
		reqBody = strings.NewReader(opts.Args.Encode())
		ctype = "application/x-www-form-urlencoded"
	}

	// Clear lastResponse in case we fail.
	b.lastResponse = nil

	// Build the http.Request, setting body and headers.
	req, err := http.NewRequest(method, uri, reqBody)
	if err != nil {
		return nil, err
	}
	req.Header.Set("User-Agent", userAgent)
	if ctype != "" {
		req.Header.Set("Content-Type", ctype)
	}
	if opts != nil && opts.Headers != nil {
		for h, v := range opts.Headers {
			req.Header.Set(h, v)
		}
	}

	// Execute the request and parse the response according to its
	// content type. Any non-200 HTTP status results in us
	// returning an error.
	resp, err := b.client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode < 200 || resp.StatusCode > 299 {
		return nil, fmt.Errorf("HTTP status %s", resp.Status)
	}

	bodyB, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("while reading response body: %w", err)
	}
	body := string(bodyB)

	var doc *goquery.Document
	switch getContentType(resp) {
	case "text/html":
		doc, err = goquery.NewDocumentFromReader(strings.NewReader(body))
		if err != nil {
			return nil, err
		}
	}

	b.lastResponse = &Response{
		URL:        resp.Request.URL,
		StatusCode: resp.StatusCode,
		Header:     resp.Header,
		Body:       body,
		Doc:        doc,
	}
	return b.lastResponse, nil
}

func (b *Browser) relURL(ustr string) (*url.URL, error) {
	u, err := url.Parse(ustr)
	if err != nil {
		return nil, err
	}

	return b.lastResponse.URL.ResolveReference(u), nil
}

// Click on a link, identified by a CSS selector. The resulting element should
// have an 'href' attribute, and we'll navigate to that.
func (b *Browser) Click(ctx context.Context, linkSelector string) (*Response, error) {
	if b.lastResponse == nil {
		return nil, errors.New("no request context")
	}
	if b.lastResponse.Doc == nil {
		return nil, errors.New("previous response was not HTML")
	}

	a := b.lastResponse.Doc.Find(linkSelector)
	if len(a.Nodes) < 1 {
		return nil, fmt.Errorf("couldn't find link \"%s\"", linkSelector)
	}
	href, _ := a.Attr("href")
	u, err := b.relURL(href)
	if err != nil {
		return nil, err
	}

	return b.Open(ctx, "GET", u.String(), nil)
}

// SubmitForm finds the specified form using a CSS selector, retrieves default
// values and modifies them with moreValues, and submits the form.
func (b *Browser) SubmitForm(ctx context.Context, formSelector string, moreValues map[string]string) (*Response, error) {
	if b.lastResponse == nil {
		return nil, errors.New("no request context")
	}
	if b.lastResponse.Doc == nil {
		return nil, errors.New("previous response was not HTML")
	}

	form := b.lastResponse.Doc.Find(formSelector)
	if form.Length() < 1 {
		return nil, fmt.Errorf("couldn't find form \"%s\"", formSelector)
	}
	if form.Length() > 1 {
		return nil, fmt.Errorf("too many results for selector \"%s\"", formSelector)
	}

	// Check that we have actually identified a <form> element.
	if !form.Is("form") {
		return nil, fmt.Errorf("selector \"%s\" does not identify a FORM element", formSelector)
	}

	// Find all the form <input> params and merge them with
	// moreValues. Does not look at <select> or <textarea> fields.
	values := make(url.Values)
	form.Find("input").Each(func(i int, s *goquery.Selection) {
		name, nok := s.Attr("name")
		value, vok := s.Attr("value")
		if nok && vok {
			values.Set(name, value)
		}
	})
	for k, v := range moreValues {
		values.Set(k, v)
	}

	// Find the form submit url.
	formURL := b.lastResponse.URL
	if s, ok := form.Attr("action"); ok {
		u, err := b.relURL(s)
		if err != nil {
			return nil, fmt.Errorf("can't parse form action= URL: %w", err)
		}
		formURL = u
	}

	formMethod := strings.ToUpper(form.AttrOr("method", "GET"))

	return b.Open(ctx, formMethod, formURL.String(), &RequestOptions{
		Args: values,
	})
}
