FROM golang:1.18 as build
ADD . /src
RUN cd /src && go build -tags netgo -o service-prober ./cmd/service-prober && strip service-prober

FROM scratch
COPY --from=build /src/service-prober /service-prober
ENTRYPOINT ["/service-prober"]

